import { Component } from '@angular/core';

@Component({
  selector: 'changeData',
  template: `<h2>Two way binding</h2>
  <p>{{ contact.name }} {{ contact.surname }}</p>
  <div>
  <label for="name">Name:</label>
  <input [(ngModel)]="contact.name" type="text" id="name">
  <label for="surname">Surame:</label>
  <input [(ngModel)]="contact.surname" type="text" id="surname">`,
})

export class ChangeData {
  contact = {name: "Kinga", surname: "Antoniak"};
}
