import { Component } from '@angular/core';
import { InputValues } from './local-values/local-values.component';
import { ToDoList } from './to-do-list/to-do-list.component';
import { ChangeData } from './change-data/change-data.component';

@Component({
  selector: 'my-app',
  directives: [InputValues, ToDoList, ChangeData],
  template: `<h1>My First Angular 2 App</h1>
  <inputValues></inputValues>
  <changeData></changeData>
  <toDoList></toDoList>`
})
export class AppComponent { }
