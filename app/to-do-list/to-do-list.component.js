"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var duties_server_1 = require('./duties.server');
var ToDoList = (function () {
    function ToDoList(dutiesService) {
        this.dutiesService = dutiesService;
        this.title = "To do list";
        this.duties = dutiesService.getDuties();
    }
    ToDoList.prototype.addNewDuty = function (val) {
        this.dutiesService.toDoList.push(val);
    };
    ToDoList = __decorate([
        core_1.Component({
            selector: 'toDoList',
            template: "<h2>{{ title }} - alterisk & service</h2>\n  <ul>\n    <li *ngFor=\"#duty of duties\">\n      {{ duty }}\n    </li>\n  </ul>\n  <input type=\"text\" #newDuty>\n  <button (click)=\"addNewDuty(newDuty.value)\">Add duty</button>",
            providers: [duties_server_1.DutiesService] //dependencies
        }), 
        __metadata('design:paramtypes', [duties_server_1.DutiesService])
    ], ToDoList);
    return ToDoList;
}());
exports.ToDoList = ToDoList;
//# sourceMappingURL=to-do-list.component.js.map