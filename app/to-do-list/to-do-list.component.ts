import { Component } from '@angular/core';
import { DutiesService } from './duties.server';

@Component({
  selector: 'toDoList',
  template: `<h2>{{ title }} - alterisk & service</h2>
  <ul>
    <li *ngFor="#duty of duties">
      {{ duty }}
    </li>
  </ul>
  <input type="text" #newDuty>
  <button (click)="addNewDuty(newDuty.value)">Add duty</button>`,
  providers: [DutiesService] //dependencies
})

export class ToDoList {
  title = "To do list";
  //duties = ["Shopping", "Laundary", "Dustong", "Vaccuming"];
  duties;

  constructor(public dutiesService: DutiesService) {
    this.duties = dutiesService.getDuties();
  }

  addNewDuty(val) {
    this.dutiesService.toDoList.push(val);
  }

}
