import { Component } from '@angular/core';

//# - referencja
@Component({ // component annotation
  selector: 'inputValues',
  template: `<h2>Render value in template & handling events</h2>
    <form>
    <label>Email</label>
    <input type="email" #inputVal>
    </form>
    <button (click)="onClick(inputVal.value)">Dodaj</button>
    <p>Witaj {{ inputVal.value }}</p>`
})

export class InputValues { //component controller
  onClick(val) {
    console.log(val);
  }


}
